import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Redirect,
} from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { DatabaseService } from './v1/database/database.service';

@Controller()
export class AppController {
  public constructor(private readonly _databaseService: DatabaseService) {
    this._databaseService.connectDatabase();
  }

  @Get()
  @ApiOperation({
    summary: 'Forwarding to this documentation.',
    description:
      'Root Route. Has no function and suffers further to this API documentation.',
  })
  @HttpCode(HttpStatus.MOVED_PERMANENTLY)
  @Redirect('/api/v1', HttpStatus.MOVED_PERMANENTLY)
  @ApiResponse({
    status: HttpStatus.MOVED_PERMANENTLY,
    description: 'Permanently moved to `/api/v1`.',
  })
  public getAPI(): void {
    // nothing
  }
}
