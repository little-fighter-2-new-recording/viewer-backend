import { Injectable } from '@nestjs/common';
import { join } from 'path';
import {
  createTestAccount,
  createTransport,
  getTestMessageUrl,
} from 'nodemailer';
import { compile } from 'handlebars';
import { readFile } from 'fs';
import { promisify } from 'util';

@Injectable()
export class MailService {
  public async testMail(): Promise<void> {
    // Generate test SMTP service account from ethereal.email
    const testAccount = await createTestAccount();

    const transporter = createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
      },
    });

    const fileContent = (
      await promisify(readFile)(
        join(process.PROJECT_ROOT, '..', 'views', 'activation.hbs'),
      )
    ).toString();

    const hbs = compile(fileContent);
    const info = await transporter.sendMail({
      from: '"Fred Foo 👻" <foo@example.com>', // sender address
      to: 'bar@example.com',
      subject: 'Easier LF2 Recording Player - Activation Code',
      text: 'Hello world?!',
      html: hbs({ username: 'test' }),
    });

    console.log('Message sent: %s', info.messageId);
    console.log('Preview URL: %s', getTestMessageUrl(info));
  }
}
