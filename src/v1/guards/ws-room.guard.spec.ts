import { WsRoomGuard } from './ws-room.guard';

describe('WsRoomGuard', () => {
  it('should be defined', () => {
    expect(new WsRoomGuard()).toBeDefined();
  });
});
