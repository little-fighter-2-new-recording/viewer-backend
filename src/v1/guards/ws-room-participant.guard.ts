import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import { verify } from 'jsonwebtoken';
import { ConfigService } from '@nestjs/config';
import { EErrorCode } from '../../core-lib/error-code.enum';

@Injectable()
export class WsRoomParticipantGuard implements CanActivate {
  public constructor(private readonly _configService: ConfigService) {}

  public canActivate(context: ExecutionContext): boolean {
    const client = context.switchToWs().getClient<Socket>();
    const result = this.canActiveFromClient(client);

    if (!result) {
      client.error(EErrorCode.roomDoesNotExit);
      client.disconnect(true);
    }

    return result;
  }

  public canActiveFromClient(client: Socket): boolean {
    const userAgent = client.handshake.headers['user-agent'];
    const token = client.handshake?.query?.tmpToken;
    const jwtPW = this._configService.get<string>('app.jwtPW');
    if (!jwtPW) throw new Error('Invalid configuration!');

    try {
      const decoded = verify(token, jwtPW) as {
        userAgent: string;
      };

      client.userAgent = userAgent;
      return decoded.userAgent === userAgent;
    } catch (e) {}

    return false;
  }
}
