import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from './auth.guard';

@Injectable()
export class OptionalAuthGuard implements CanActivate {
  private readonly _authGuard: AuthGuard;
  public constructor(private readonly _configService: ConfigService) {
    this._authGuard = new AuthGuard(this._configService);
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    try {
      await this._authGuard.canActivate(context);
    } catch (e) {}

    return true;
  }
}
