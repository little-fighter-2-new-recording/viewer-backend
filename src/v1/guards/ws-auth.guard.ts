import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';
import { AuthGuard } from './auth.guard';
import UserDocument from '../models/documents/user.document';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class WsAuthGuard implements CanActivate {
  private readonly _authGuard: AuthGuard;

  public constructor(private readonly configService: ConfigService) {
    this._authGuard = new AuthGuard(configService);
  }

  public async authUserFromSocketClient(client: Socket): Promise<boolean> {
    let user: UserDocument | null = null;
    try {
      user = await this._authGuard.authUser(
        AuthGuard.extractTokenFromBearerHeader(client.handshake?.query?.token),
        client.handshake.headers['user-agent'] ?? '',
      );
    } catch (e) {}

    if (!user) {
      console.debug('failed');
      client.disconnect(true);
      return false;
    }

    client.user = user ?? undefined;
    return !!user;
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const client = context.switchToWs().getClient<Socket>();
    client.token = client.handshake.headers['user-agent'] ?? '';

    return this.authUserFromSocketClient(client);
  }
}
