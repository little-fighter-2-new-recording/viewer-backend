import { HasRootPermissionGuard } from './has-root-permission.guard';

describe('HasRootPermissionGuard', () => {
  it('should be defined', () => {
    expect(new HasRootPermissionGuard()).toBeDefined();
  });
});
