import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Request } from 'express';
import UserDocument from '../models/documents/user.document';
import { verify } from 'jsonwebtoken';
import UserDBM from '../models/schemas/user.schema';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthGuard implements CanActivate {
  public constructor(private readonly _configService: ConfigService) {}

  /**
   *
   * @param headerContent Should be the return of `req.header('Authorization')` without any checks before.
   */
  public static extractTokenFromBearerHeader(
    headerContent: string | undefined,
  ): string | null {
    if (!headerContent) return null;
    const token = headerContent.replace(/bearer\s+/i, ''); // regex replace bearer(SPACE) (case insensitive)
    if (!token) return null;

    return token;
  }

  public async authAndSet(
    tokenWithHeaderPayload: string,
    userInfo: UserInformation,
  ): Promise<boolean> {
    const token = AuthGuard.extractTokenFromBearerHeader(
      tokenWithHeaderPayload,
    );
    const user = await this.authUser(token, userInfo.userAgent);

    if (!user) throw new UnauthorizedException('Authenticate failed.');

    userInfo.user = user;
    userInfo.token = token as string;
    return !!user;
  }

  public async authUser(
    token: string | null,
    userAgent: string,
  ): Promise<UserDocument | null> {
    if (!token) return null;

    const jwtPW = this._configService.get<string>('app.jwtPW');
    if (!jwtPW) throw new Error('Invalid configuration!');

    try {
      const decoded = verify(token, jwtPW) as {
        _id: string;
        userAgent: string;
      };
      const user = await UserDBM.findOne({
        _id: decoded._id,
        tokens: token,
      });

      if (!user) return null;

      if (decoded.userAgent === userAgent) return user;
    } catch (e) {
      // ignore exceptions
    }
    return null;
  }

  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<Request>();
    return await this.authAndSet(req.header('Authorization') ?? '', req);
  }
}
