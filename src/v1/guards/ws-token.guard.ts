import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { verify } from 'jsonwebtoken';
import { Request } from 'express';
import { AuthGuard } from './auth.guard';
import { RoomService } from '../room/room.service';

@Injectable()
export class WsTokenGuard implements CanActivate {
  public constructor(
    private readonly _configService: ConfigService,
    private readonly _roomService: RoomService,
  ) {}

  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<Request>();
    try {
      const token = AuthGuard.extractTokenFromBearerHeader(
        request.header('Authorization') ?? '',
      );
      if (!token) throw new UnauthorizedException('Authenticate failed.');
      const jwtPW = this._configService.get<string>('app.jwtPW');
      if (!jwtPW) throw new Error('Invalid configuration!');
      const { httpID } = verify(token, jwtPW) as {
        httpID: string;
      };

      request.httpID = httpID;
      request.room = this._roomService.getRoomToHttpID(httpID);
      if (!request.room) throw new Error();

      return !!request.room;
    } catch (e) {}
    throw new UnauthorizedException('Authenticate failed.');
  }
}
