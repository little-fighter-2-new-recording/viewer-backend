import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { Request } from 'express';
import { EPermission } from '../../core-lib/dtos/permission.enum';

@Injectable()
export class HasRootPermissionGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<Request>();
    if (!request.room || !request.httpID) throw new ForbiddenException();

    if (request.room.ownerID === request.httpID) return true;

    for (const participant of request.room.participants) {
      if (participant.httpID === request.httpID) {
        if (
          (participant.permission !== undefined &&
            participant.permission === EPermission.root) || // the participant has root permissions
          (participant.permission === undefined &&
            request.room.permission === EPermission.root) // the participant has NO permissions, so check the global room permissions
        )
          return true;

        break;
      }
    }

    throw new ForbiddenException();
  }
}
