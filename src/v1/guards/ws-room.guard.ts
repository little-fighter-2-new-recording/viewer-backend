import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Socket } from 'socket.io';
import { verify } from 'jsonwebtoken';
import { RoomService } from '../room/room.service';
import { EErrorCode } from '../../core-lib/error-code.enum';

@Injectable()
export class WsRoomGuard implements CanActivate {
  public constructor(
    private readonly _configService: ConfigService,
    private readonly _roomService: RoomService,
  ) {}

  public canActivate(context: ExecutionContext): boolean {
    const client = context.switchToWs().getClient<Socket>();
    const result = this.canActiveFromClient(client);

    if (!result) {
      client.error(EErrorCode.roomDoesNotExit);
      client.disconnect(true);
    }

    return result;
  }

  public canActiveFromClient(client: Socket): boolean {
    const token = client.handshake?.query?.tmpToken;
    const jwtPW = this._configService.get<string>('app.jwtPW');
    if (!jwtPW) throw new Error('Invalid configuration!');

    try {
      const { httpID } = verify(token, jwtPW) as {
        httpID: string;
      };

      client.httpID = httpID;
      client.room = this._roomService.getRoomToHttpID(httpID);
      return !!client.room;
    } catch (e) {}

    return false;
  }
}
