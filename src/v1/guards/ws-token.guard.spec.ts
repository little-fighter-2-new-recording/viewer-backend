import { WsTokenGuard } from './ws-token.guard';

describe('WsTokenGuard', () => {
  it('should be defined', () => {
    expect(new WsTokenGuard()).toBeDefined();
  });
});
