import { WsRoomParticipantGuard } from './ws-room-participant.guard';

describe('WsRoomParticipantGuard', () => {
  it('should be defined', () => {
    expect(new WsRoomParticipantGuard()).toBeDefined();
  });
});
