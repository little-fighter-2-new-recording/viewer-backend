import {
  BadRequestException,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiConsumes,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express, Request, Response } from 'express';
import { Types } from 'mongoose';
import { ApiFile } from '../decorators/api-file';
import RecordingDBM from '../models/schemas/recording.schema';
import { AuthGuard } from '../guards/auth.guard';
import { RecordingService } from './recording.service';
import { OptionalPipe } from '../pipes/optional.pipe';
import { MINIMAL_RECORDINGS_PER_PAGE } from './constants';
import ParseObjectIdPipe from '../pipes/parse-object-id.pipe';
import RecordingListDTO from './models/recording-list.dto';

@Controller('recording')
@ApiTags('recording')
export class RecordingController {
  public constructor(private readonly _service: RecordingService) {}

  @Get('list')
  @ApiQuery({
    name: 'page',
    type: 'Number',
    required: false,
  })
  @ApiQuery({
    name: 'pageSize',
    type: 'Number',
    required: false,
  })
  @ApiOkResponse({
    type: RecordingListDTO,
  })
  public async getRecordings(
    @Req() req: Request,
    @Query('page', new OptionalPipe<ParseIntPipe>(ParseIntPipe))
    page: number | undefined,
    @Query('pageSize', new OptionalPipe<ParseIntPipe>(ParseIntPipe))
    pageSize: number | undefined,
  ): Promise<RecordingListDTO> {
    console.log(req.get('origin'), req.baseUrl, req.hostname);
    return await this._service.getRecordings(
      page ?? 0,
      pageSize ?? MINIMAL_RECORDINGS_PER_PAGE,
    );
  }

  @UseGuards(AuthGuard)
  @Post('upload/:title/:author')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth()
  @ApiParam({
    name: 'title',
    type: 'String',
    required: true,
  })
  @ApiParam({
    name: 'author',
    type: 'String',
    required: false,
  })
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @ApiUnauthorizedResponse({
    description: 'If the user is not logged in.',
  })
  @ApiBadRequestResponse()
  public async uploadRecording(
    @Req() req: Request,
    @UploadedFile() file: Express.Multer.File,
    @Param('title') title: string,
    @Param('author') author: string | undefined,
  ): Promise<void> {
    await this._service.uploadRecording(file, req.user!, title, author);
  }

  @Get('/:id')
  @ApiParam({
    name: 'id',
    type: 'String',
    description: 'The ID of the recording file',
    required: true,
  })
  @ApiOkResponse({
    type: Uint8Array,
  })
  @ApiBadRequestResponse()
  public async getRecording(
    @Res() res: Response,
    @Param('id', ParseObjectIdPipe) id: Types.ObjectId,
  ): Promise<void> {
    const record = await RecordingDBM.findOne({ _id: id });
    if (!record) throw new BadRequestException();

    res.set('Content-Type', 'plain/text');
    res.set('Content-Disposition', 'attachment; filename=test.nlfr');
    res.send(record.file);
  }

  @UseGuards(AuthGuard)
  @Delete('/:id')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Deletes a public recording file.',
    description:
      'Deletes a public recording file if the logged-in user is the owner/uploader of the file.',
  })
  @ApiParam({
    name: 'id',
    type: 'String',
    description: 'The ID of the recording file',
    required: true,
  })
  @ApiUnauthorizedResponse({
    description:
      'If the user does not have permission to delete the file or if the user is not logged in.',
  })
  @ApiBadRequestResponse()
  public async deleteRecording(
    @Req() req: Request,
    @Param('id', ParseObjectIdPipe) id: Types.ObjectId,
  ): Promise<void> {
    await this._service.deleteRecording(id, req.user!);
  }

  /*
  Multiple file upload
  @Post('upload')
  @ApiConsumes('multipart/form-data')
  @ApiMultiFile()
  @UseInterceptors(FilesInterceptor('files'))
  public async uploadFile(
    @UploadedFiles() files: Express.Multer.File[],
  ): Promise<void> {
    console.log(files);
  }
  */
}
