import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import RecordingDBM from '../models/schemas/recording.schema';
import {
  MAXIMAL_RECORDINGS_PER_PAGE,
  MINIMAL_RECORDINGS_PER_PAGE,
} from './constants';
import RecordingItemDTO from './models/recording-item.dto';
import RecordingListDTO from './models/recording-list.dto';
import {
  RECORDING_TITLE_MIN_LENGTH,
  USERNAME_MIN_LENGTH,
} from '../../core-lib/dtos/constants';
import { Lf2RecordingFormatPipe } from '../pipes/lf2-recording-format.pipe';
import RecordingDocument from '../models/documents/recording.document';
import UserDocument from '../models/documents/user.document';
import { Types } from 'mongoose';

@Injectable()
export class RecordingService {
  public async getRecordings(
    pageIndex: number,
    pageSize: number,
  ): Promise<RecordingListDTO> {
    if (pageIndex < 0) pageIndex = 0;

    if (
      pageSize < MINIMAL_RECORDINGS_PER_PAGE ||
      pageSize > MAXIMAL_RECORDINGS_PER_PAGE
    )
      throw new BadRequestException('The page size is out of range!');

    const amountOfRecordings = await RecordingDBM.countDocuments();
    let maxIndex: number;
    if (amountOfRecordings > 0 && amountOfRecordings % pageSize === 0)
      maxIndex = amountOfRecordings / pageSize - 1;
    else maxIndex = Math.floor(amountOfRecordings / pageSize);

    const result: Partial<RecordingListDTO> = {
      pageSize,
      pageIndex,
      maxPageIndex: maxIndex,
      recordings: [],
    };

    const list = await RecordingDBM.find()
      .select('author title uploader uploadDate fileSize')
      .limit(pageSize)
      .skip(pageSize * pageIndex)
      .sort({ uploadDate: 'asc' })
      .exec();

    for (const r of list) {
      await r.populate('uploader').execPopulate();
      result.recordings!.push({
        title: r.title,
        author: r.author,
        uploader: r.uploader.username,
        fileSize: r.fileSize,
        uploadDate: r.uploadDate,
      } as RecordingItemDTO);
    }

    return new RecordingListDTO(result);
  }

  public async uploadRecording(
    recordingFile: Express.Multer.File,
    user: UserDocument,
    title: string,
    author: string | undefined,
  ): Promise<void> {
    if (!title || title.length < RECORDING_TITLE_MIN_LENGTH)
      throw new BadRequestException('');

    if (!author || author.length < USERNAME_MIN_LENGTH) author = user.username;

    new Lf2RecordingFormatPipe().transform(recordingFile); // check pipe, `UploadedFile` doesn't support pipe currently

    const recording = new RecordingDBM({
      uploader: user,
      author,
      file: recordingFile.buffer,
      title: title,
      uploadDate: Date.now(),
      fileSize: recordingFile.buffer.length,
    } as RecordingDocument);
    await recording.save();
  }

  public async deleteRecording(
    recordingID: Types.ObjectId,
    user: UserDocument,
  ): Promise<void> {
    const recording = await RecordingDBM.findOne({ _id: recordingID });
    if (!recording) throw new BadRequestException();

    if (!recording.uploader._id.equals(user._id))
      throw new UnauthorizedException();

    await recording.remove();
  }
}
