import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsNumber,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/recording-list';
import RecordingItemDTO from '../../../core-lib/dtos/recording-item.dto';
import { MINIMAL_RECORDINGS_PER_PAGE } from '../constants';

export default class RecordingListDTO extends BaseDTO {
  @ApiProperty({
    description: '',
    type: RecordingItemDTO,
    isArray: true,
  })
  @ValidateNested()
  @IsArray()
  public readonly recordings!: RecordingItemDTO[];

  @ApiProperty({
    description: '',
  })
  @IsNumber()
  @Min(0)
  public readonly pageIndex!: number;

  @ApiProperty({
    description: '',
  })
  @IsNumber()
  @MinLength(MINIMAL_RECORDINGS_PER_PAGE)
  public readonly pageSize!: number;

  @ApiProperty({
    description: '',
  })
  @IsNumber()
  @Min(0)
  public readonly maxPageIndex!: number;

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(model: Partial<RecordingListDTO>) {
    super(model);
  }
}
