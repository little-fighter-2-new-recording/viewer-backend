import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString, Min, MinLength } from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/recording-item.dto';
import {
  RECORDING_TITLE_MIN_LENGTH,
  USERNAME_MIN_LENGTH,
} from '../../../core-lib/dtos/constants';

export default class RecordingItemDTO extends BaseDTO {
  @ApiProperty({
    description: '',
  })
  @IsString()
  @MinLength(RECORDING_TITLE_MIN_LENGTH)
  public readonly title!: string;

  @ApiProperty({
    description: '',
  })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  public readonly uploader!: string;

  @ApiProperty({
    description: '',
  })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  public readonly author!: string;

  @ApiProperty({
    description: '',
  })
  @IsNumber()
  @Min(0)
  public readonly uploadDate!: number;

  @ApiProperty({
    description: '',
  })
  @IsNumber()
  @Min(0)
  public readonly fileSize!: number;

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(model: Partial<RecordingItemDTO>) {
    super(model);
  }
}
