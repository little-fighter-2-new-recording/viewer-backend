import { Module } from '@nestjs/common';
import { RecordingController } from './recording.controller';
import { ConfigModule } from '@nestjs/config';
import { RecordingService } from './recording.service';

@Module({
  controllers: [RecordingController],
  imports: [ConfigModule],
  providers: [RecordingService],
})
export class RecordingModule {}
