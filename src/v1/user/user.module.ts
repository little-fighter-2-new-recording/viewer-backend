import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserBridgeService } from './user-bridge.service';

@Module({
  controllers: [UserController],
  providers: [UserService, UserBridgeService],
  imports: [ConfigModule],
})
export class UserModule {}
