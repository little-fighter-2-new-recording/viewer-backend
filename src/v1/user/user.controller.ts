import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  NotImplementedException,
  Post,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { Error as MError } from 'mongoose';
import ValidationPipe from '../pipes/validation.pipe';
import RegisterDTO from './models/register.dto';
import { UserService } from './user.service';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotImplementedResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import LoginResultDTO from './models/login-result.dto';
import LoginDataDTO from './models/login-data.dto';
import { UserBridgeService } from './user-bridge.service';

@Controller('user')
@ApiTags('user')
export class UserController {
  public constructor(
    private readonly _service: UserService,
    private readonly _bridge: UserBridgeService,
  ) {}

  @Post('login')
  @HttpCode(HttpStatus.CREATED)
  @ApiCreatedResponse({
    type: LoginResultDTO,
    description: '...',
  })
  @ApiBody({
    type: LoginDataDTO,
  })
  @ApiBadRequestResponse()
  public async login(
    @Req() req: Request,
    @Body(new ValidationPipe<LoginDataDTO>(LoginDataDTO)) user: LoginDataDTO,
  ): Promise<LoginResultDTO> {
    return await this._bridge.login(user, req.userAgent);
  }

  @Post('register')
  @HttpCode(HttpStatus.NOT_IMPLEMENTED)
  @ApiOperation({
    deprecated: true,
    description: 'This function is not yet implement or active',
    summary: 'This function is not yet implement or active',
  })
  @ApiNotImplementedResponse({
    description: 'This function is not yet implement or active',
  })
  public async register(
    @Body(new ValidationPipe<RegisterDTO>(RegisterDTO)) user: RegisterDTO,
  ): Promise<void> {
    throw new NotImplementedException(
      'This function is not yet implement or active',
    );

    try {
      await this._service.register(user);
    } catch (ex) {
      if (ex instanceof MError.ValidationError)
        throw new BadRequestException(
          'Username or email address exists already!',
        );

      throw ex;
    }
  }
}
