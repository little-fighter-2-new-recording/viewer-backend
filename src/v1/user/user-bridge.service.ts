import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import validator from 'validator';
import { sign as JWTSign } from 'jsonwebtoken';
import LoginDataDTO from './models/login-data.dto';
import LoginResultDTO from './models/login-result.dto';
import UserDBM from '../models/schemas/user.schema';
import UserDocument from '../models/documents/user.document';

@Injectable()
// why not in userService my boi?!
export class UserBridgeService {
  public constructor(private readonly _configService: ConfigService) {}

  /**
   * This function serves to create a token for a user on login or for the tempUser on creation
   * of the baseUser object.
   * @param user This represents the user the token will be assigned to and is part of the tokens creation.
   * @param userAgent This represents the userAgent information of the user the token is created for.
   * @return Returns the newly formed token.
   */
  public async createLoginToken(
    user: UserDocument,
    userAgent: string,
  ): Promise<string> {
    const jwtPW = this._configService.get<string>('app.jwtPW');
    if (!jwtPW) throw new Error('Invalid configuration!');
    const token = JWTSign({ _id: user._id.toString(), userAgent }, jwtPW);
    user.tokens.push(token);
    await user.updateOne({ $push: { tokens: token } });

    return token;
  }

  /**
   * This functions handles the login of a user. It takes the information from a user and tries to
   * verify it in order for the user to access his profile on the server.
   * @param usernameOrEmail Contains the username or user-email-address that the user provides the server with when logging in.
   * @param password Contains the password that the user provides the server with when logging in.
   * @param userAgent This represents the userAgent information which is being sent via the request.
   * @return Returns the encoded JSON token as a string.
   */
  public async login(
    { user: usernameOrEmail, password }: LoginDataDTO,
    userAgent: string,
  ): Promise<LoginResultDTO> {
    try {
      let user;
      if (validator.isEmail(usernameOrEmail)) {
        user = await UserDBM.findByEmailAndPassword(usernameOrEmail, password);
      } else {
        user = await UserDBM.findByUsernameAndPassword(
          usernameOrEmail,
          password,
        );
      }

      return {
        username: user.username,
        token: await this.createLoginToken(user, userAgent),
      } as LoginResultDTO;
    } catch (error) {
      if (
        error instanceof BadRequestException ||
        error instanceof InternalServerErrorException
      ) {
        throw error;
      } else if (error instanceof Error) {
        throw new BadRequestException('Wrong email, username or password');
      }
      throw new InternalServerErrorException('Something went wrong!');
    }
  }
}
