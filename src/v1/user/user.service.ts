import { Injectable } from '@nestjs/common';
import UserDBM from '../models/schemas/user.schema';
import RegisterDTO from './models/register.dto';

@Injectable()
export class UserService {
  public async register(user: RegisterDTO): Promise<void> {
    const userDBM = new UserDBM(user);
    await userDBM.save();
  }
}
