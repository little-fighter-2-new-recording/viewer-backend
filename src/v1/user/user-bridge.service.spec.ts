import { Test, TestingModule } from '@nestjs/testing';
import { UserBridgeService } from './user-bridge.service';

describe('UserBridgeService', () => {
  let service: UserBridgeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserBridgeService],
    }).compile();

    service = module.get<UserBridgeService>(UserBridgeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
