import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/register.dto';
import {
  PASSWORD_MIN_LENGTH,
  USERNAME_MIN_LENGTH,
} from '../../../core-lib/dtos/constants';

/**
 * Represents a user.
 */
export default class RegisterDTO extends BaseDTO {
  /** Represents the unique username of a registered user. */
  @ApiProperty({
    description:
      'The given name with witch to register the user on the server and database.',
  })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  public username!: string;

  /** This is the email address given that will be linked to the profile of that user. */
  @ApiProperty({
    description:
      'The email address given that will be linked to the profile of that user.',
  })
  @IsEmail()
  public email!: string;

  /** Represents the password given by the user for his profile. */
  @ApiProperty({
    description:
      "The password given by the user for his profile. Is being encrypted before stored in database to enhance it's security and safety.",
  })
  @IsString()
  @MinLength(PASSWORD_MIN_LENGTH)
  public password!: string;

  public constructor(model: Partial<RegisterDTO>) {
    super(model);
  }
}
