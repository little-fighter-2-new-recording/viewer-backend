import { ApiProperty } from '@nestjs/swagger';
import { IsString, MinLength } from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/login-result.dto';
import { USERNAME_MIN_LENGTH } from '../../../core-lib/dtos/constants';

/**
 * Represents the result dto model for a successful login.
 */
export default class LoginResultDTO extends BaseDTO {
  /** The token of the user. */
  @ApiProperty({
    description: 'The token of the user.',
  })
  @IsString()
  public token!: string;

  /** The token of the user. */
  @ApiProperty({
    description: 'The name of the user.',
  })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  public username!: string;

  public constructor(model: Partial<LoginResultDTO>) {
    super(model);
  }
}
