import { IoAdapter } from '@nestjs/platform-socket.io';
import { Request } from 'express';
import { INestApplicationContext } from '@nestjs/common';

// Doesn't work... God damn stupid comment on GitHub and documentation - WHY?!

// https://github.com/nestjs/nest/issues/882
// https://stackoverflow.com/a/65587828 ...
export class AuthWsIoAdapter extends IoAdapter {
  private readonly _originURL: string | RegExp;
  public constructor(
    app: INestApplicationContext | any,
    originURL: string | RegExp,
  ) {
    super(app);

    this._originURL = originURL;
  }

  public createIOServer(port: number, options?: any): any {
    options.origins = [this._originURL];
    options.handlePreflightRequest = (req: any, res: any) => {
      console.log(req, res);
      res.writeHead(200, {
        'Access-Control-Allow-Origin': this._originURL,
        'Access-Control-Allow-Methods': 'GET,POST',
        'Access-Control-Allow-Headers': 'my-custom-header',
        'Access-Control-Allow-Credentials': true,
      });
      res.end();
    };
    options.allowRequest = async (
      request: Request,
      allowFunction: (
        errCode: string | number | null,
        success: boolean,
      ) => void,
    ) => {
      //    console.log('RES', request);
      return allowFunction(null, true);
      // return allowFunction(302, false);
    };
    console.log(this._originURL);
    return super.createIOServer(port, options);
  }
}
