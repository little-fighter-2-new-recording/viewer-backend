import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { v4 as uuidv4 } from 'uuid';
import { sign as JWTSign } from 'jsonwebtoken';
import Room from './models/room.model';
import RoomCreatorDTO from './models/room-creator.dto';
import RoomCreatedDTO from './models/room-created.dto';
import UserDocument from '../models/documents/user.document';
import RoomJoinedDTO from './models/room-joined.dto';
import Participant from './models/participant.model';
import { EPermission } from '../../core-lib/dtos/permission.enum';
import RoomParticipantDTO from './models/room-participant.dto';
import BinaryParser from '../../core-lib/parsers/binary-parser';
import { Server } from 'socket.io';
import { EVENTS_ROOM_RECORDING_CHANGED } from '../../core-lib/events.constants';
import { EErrorCode } from '../../core-lib/error-code.enum';

/** Defines how long a room will remain without any action before it is deleted (in milliseconds). */
const ROOM_CLEARER = 1000 * 60 * 60;

/** Defines the interval in which it is checked whether a room can be deleted. */
const ROOM_CLEARER_INTERVAL = 1000 * 60 * 5;

@Injectable()
export class RoomService {
  public wsServer: Server | undefined;

  private readonly _rooms: Room[] = [];

  private readonly _roomClearer: NodeJS.Timeout;

  public constructor(private readonly _configService: ConfigService) {
    this._roomClearer = setInterval(
      this.clearRooms.bind(this),
      ROOM_CLEARER_INTERVAL,
    );
  }

  public get(roomID: string): Room | undefined {
    return this._rooms.find((r) => r.id === roomID);
  }

  public createRoom(
    room: RoomCreatorDTO,
    userAgent: string,
    user: UserDocument | undefined,
  ): RoomCreatedDTO {
    const roomID = uuidv4();
    const owner = this.createParticipant(userAgent);

    const roomObj = new Room(
      roomID,
      owner.httpID,
      room.name,
      room.password,
      room.defaultPermission,
    );
    roomObj.participants.push(new Participant(owner.httpID, user?.username));
    roomObj.participants[0].permission = EPermission.root;

    this._rooms.push(roomObj);

    return new RoomCreatedDTO({
      wsToken: owner.wsToken,
      roomID,
      id: owner.httpID,
    });
  }

  public joinRoom(
    roomID: string,
    password: string | undefined,
    userAgent: string,
    user: UserDocument | undefined = undefined,
  ): RoomJoinedDTO {
    const room = this.get(roomID);
    if (!room || room.password !== password)
      throw new BadRequestException(
        "Room doesn't exist or password is invalid",
      );

    const participant = this.createParticipant(userAgent);
    room.participants.push(new Participant(participant.httpID, user?.username));

    return new RoomJoinedDTO({
      id: participant.httpID,
      wsToken: participant.wsToken,

      permission: room.permission,

      title: room.name,
      fps: room.fps,
      isPause: room.isPause,
      isReverse: room.isReverse,
      ownerID: room.ownerID,
      participants: room.participants.map(
        (p) =>
          new RoomParticipantDTO({
            username: p.username,
            permission: p.permission,
            id: p.httpID,
          }),
      ),
    });
  }

  public setRecording(
    room: Room,
    uploader: string,
    recordingBinary: Uint8Array | undefined,
  ): void {
    try {
      if (recordingBinary)
        room.recording = new BinaryParser(recordingBinary).parse();
      else room.recording = undefined;

      room.recordingBinary = recordingBinary;
      /* for version 1.1:
              new RecordingChangedDTO({
          bg: room.recording.bg,
          backgroundIndex: room.recording.backgroundIndex,
          players: room.recording.players,
          author: room.recording.author,
          info: room.recording.info,
          difficulty: room.recording.difficulty,
          mode: room.recording.mode,
          objects: room.recording.objects,
        }),
       */
    } catch (e) {
      throw new BadRequestException();
    }

    if (!this.wsServer) return;

    const uploaderSocket = room.participants.find((p) => p.httpID === uploader);
    if (!uploaderSocket || !uploaderSocket.socket)
      this.wsServer.in(room.id).emit(EVENTS_ROOM_RECORDING_CHANGED);
    else uploaderSocket.socket.broadcast.emit(EVENTS_ROOM_RECORDING_CHANGED);
  }

  public getRoomToHttpID(httpID: string): Room | undefined {
    for (const room of this._rooms) {
      if (room.ownerID === httpID) return room;

      for (const participant of room.participants) {
        if (participant.httpID === httpID) return room;
      }
    }

    return undefined;
  }

  private clearRooms(): void {
    const deletionTime = Date.now() - ROOM_CLEARER;
    const oldLen = this._rooms.length;
    let index = 0;
    while (index < this._rooms.length) {
      const room = this._rooms[index];
      if (room.lastAction - deletionTime > 0) {
        ++index;
        return;
      }

      this._rooms[index].participants.forEach((p) => {
        p.socket?.error(EErrorCode.roomDoesNotExit);
        p.socket?.disconnect(true);
      });
      this._rooms.splice(index, 1);
    }

    if (oldLen !== this._rooms.length) {
      console.log('Some rooms was removed', this._rooms);
    }
  }

  private createParticipant(
    userAgent: string,
  ): { httpID: string; wsToken: string } {
    const httpID = uuidv4();
    const jwtPW = this._configService.get<string>('app.jwtPW');
    if (!jwtPW) throw new Error('Invalid configuration!');

    return {
      httpID,
      wsToken: JWTSign({ httpID, userAgent }, jwtPW),
    };
  }
}
