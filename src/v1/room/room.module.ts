import { Module } from '@nestjs/common';
import { RoomController } from './room.controller';
import { RoomService } from './room.service';
import { RoomGateway } from './room.gateway';
import { ConfigModule } from '@nestjs/config';

@Module({
  controllers: [RoomController],
  providers: [RoomService, RoomGateway],
  imports: [ConfigModule],
})
export class RoomModule {}
