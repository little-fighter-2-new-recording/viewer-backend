import {
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ConfigService } from '@nestjs/config';
import { EErrorCode } from '../../core-lib/error-code.enum';
import { RoomService } from './room.service';
import { WsRoomParticipantGuard } from '../guards/ws-room-participant.guard';
import { WsRoomGuard } from '../guards/ws-room.guard';
import {
  EVENTS_ROOM_FPS_CHANGED,
  EVENTS_ROOM_PARTICIPANTS_CHANGED,
  EVENTS_ROOM_PLAY_STATE_CHANGED,
  MESSAGE_ROOM_GET_OPTIONS,
  MESSAGE_ROOM_HAS_RECORDING,
  MESSAGE_ROOM_SET_FPS,
  MESSAGE_ROOM_SET_OPTIONS,
  MESSAGE_ROOM_SET_PLAY_STATE,
} from '../../core-lib/events.constants';
import { UseGuards } from '@nestjs/common';
import RoomParticipantDTO from './models/room-participant.dto';
import Room from './models/room.model';
import RoomOptionsDTO from '../../core-lib/dtos/room-options.dto';
import RoomPlayStateDTO from '../../core-lib/dtos/room-play-state.dto';

@WebSocketGateway()
export class RoomGateway
  implements OnGatewayConnection<Socket>, OnGatewayDisconnect<Socket> {
  @WebSocketServer()
  public server: Server | undefined;

  public constructor(
    private readonly _configService: ConfigService,
    private readonly _service: RoomService,
  ) {
    this._service.wsServer = this.server;
  }

  private static getWSFriendlyParticipants(room: Room): RoomParticipantDTO[] {
    return room.participants.map(
      (p) =>
        new RoomParticipantDTO({
          username: p.username,
          permission:
            p.permission !== undefined ? p.permission : room!.permission,
          id: p.httpID,
        }),
    );
  }

  public handleConnection(client: Socket): void {
    const failed =
      !new WsRoomParticipantGuard(this._configService).canActiveFromClient(
        client,
      ) ||
      !new WsRoomGuard(this._configService, this._service).canActiveFromClient(
        client,
      );

    // password check already in rest api, but if you want check it, then here:       (client.room &&
    //         client.room.ownerID !== client.httpID &&
    //         client.room.password !== client.handshake?.query?.password)
    if (failed || !client.room) {
      client.error(EErrorCode.roomDoesNotExit);
      client.disconnect(true);
      return;
    }

    for (const participant of client.room.participants.filter(
      (p) => p.httpID === client.httpID,
    ))
      participant.socket = client;

    if (
      client.httpID === client.room.creatorID &&
      client.room.creatorID !== client.room.ownerID
    )
      client.room.ownerID = client.httpID;

    client.join(client.room.id);

    this.server
      ?.in(client.room.id)
      .emit(
        EVENTS_ROOM_PARTICIPANTS_CHANGED,
        RoomGateway.getWSFriendlyParticipants(client.room!),
      );
    console.debug('login successful', client.httpID);
  }

  public handleDisconnect(client: Socket): void {
    // remark: The client room list is already empty to this moment...

    // call the guards to set the properties
    new WsRoomParticipantGuard(this._configService).canActiveFromClient(client);
    new WsRoomGuard(this._configService, this._service).canActiveFromClient(
      client,
    );

    client.room!.participants = client.room!.participants.filter(
      (p) => p.httpID !== client.httpID,
    );
    this.server
      ?.in(client.room!.id)
      .emit(
        EVENTS_ROOM_PARTICIPANTS_CHANGED,
        RoomGateway.getWSFriendlyParticipants(client.room!),
      );
  }

  @SubscribeMessage(MESSAGE_ROOM_HAS_RECORDING)
  @UseGuards(WsRoomParticipantGuard, WsRoomGuard)
  public handleHasRecordingMessage(client: Socket): boolean {
    return !!client.room!.recording;
  }

  @SubscribeMessage(MESSAGE_ROOM_GET_OPTIONS)
  @UseGuards(WsRoomParticipantGuard, WsRoomGuard)
  public handleGetRoomOptionsMessage(client: Socket): RoomOptionsDTO {
    return new RoomOptionsDTO({
      fps: client.room!.fps,
      currentIndex: client.room!.currentIndex,
      isPause: client.room!.isPause,
    });
  }

  @SubscribeMessage(MESSAGE_ROOM_SET_OPTIONS)
  @UseGuards(WsRoomParticipantGuard, WsRoomGuard)
  public handleSetRoomOptionsMessage(
    client: Socket,
    data: RoomOptionsDTO,
  ): void {
    client.room!.isPause = data.isPause;
    client.room!.fps = data.fps;
    client.room!.currentIndex = data.currentIndex;
  }

  @SubscribeMessage(MESSAGE_ROOM_SET_FPS)
  @UseGuards(WsRoomParticipantGuard, WsRoomGuard)
  public handleSetFPSMessage(client: Socket, fps: number): void {
    client.room!.fps = fps;
    client.broadcast.emit(EVENTS_ROOM_FPS_CHANGED, fps);
  }

  @SubscribeMessage(MESSAGE_ROOM_SET_PLAY_STATE)
  @UseGuards(WsRoomParticipantGuard, WsRoomGuard)
  public handleSetPlayStateMessage(
    client: Socket,
    playState: RoomPlayStateDTO,
  ): void {
    client.room!.isPause = playState.isPause;
    client.room!.currentIndex = playState.currentIndex;
    client.broadcast.emit(EVENTS_ROOM_PLAY_STATE_CHANGED, playState);
  }
}
