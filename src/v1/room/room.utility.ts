import { Socket } from 'socket.io';
import { validate } from 'uuid';

export function getRoomIDFromSocket(client: Socket): string | undefined {
  try {
    const roomID = client.handshake?.query?.roomID;
    if (!roomID || !validate(roomID)) return undefined;

    return roomID;
  } catch (e) {}

  return undefined;
}
