import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Put,
  Req,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Express, Request, Response } from 'express';
import ValidationPipe from '../pipes/validation.pipe';
import RoomCreatorDTO from './models/room-creator.dto';
import RoomCreatedDTO from './models/room-created.dto';
import { RoomService } from './room.service';
import { OptionalAuthGuard } from '../guards/optional-auth.guard';
import RoomJoinDTO from './models/room-join.dto';
import RoomJoinedDTO from './models/room-joined.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiFile } from '../decorators/api-file';
import { WsTokenGuard } from '../guards/ws-token.guard';
import { HasRootPermissionGuard } from '../guards/has-root-permission.guard';

@Controller('room')
@ApiTags('room')
export class RoomController {
  public constructor(private readonly _roomService: RoomService) {}

  @UseGuards(OptionalAuthGuard)
  @Post('/create')
  @ApiBearerAuth()
  @ApiBody({
    type: RoomCreatorDTO,
  })
  @ApiCreatedResponse({
    type: RoomCreatedDTO,
  })
  @ApiBadRequestResponse()
  public createRoom(
    @Body(new ValidationPipe<RoomCreatorDTO>(RoomCreatorDTO))
    room: RoomCreatorDTO,
    @Req() request: Request,
  ): RoomCreatedDTO {
    return this._roomService.createRoom(room, request.userAgent, request.user);
  }

  @UseGuards(OptionalAuthGuard)
  @Put('/join')
  @HttpCode(HttpStatus.ACCEPTED)
  @ApiBearerAuth()
  @ApiBody({ type: RoomJoinDTO })
  @ApiCreatedResponse({
    type: RoomJoinedDTO,
  })
  @ApiBadRequestResponse()
  public joinRoom(
    @Body(new ValidationPipe<RoomJoinDTO>(RoomJoinDTO)) room: RoomJoinDTO,
    @Req() request: Request,
  ): RoomJoinedDTO {
    return this._roomService.joinRoom(
      room.roomID,
      room.password,
      request.userAgent,
      request.user,
    );
  }

  @UseGuards(WsTokenGuard, HasRootPermissionGuard)
  @Put('/recording')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiFile()
  @ApiUnauthorizedResponse({
    description: 'If the web socket token invalid.',
  })
  @ApiBadRequestResponse({
    description: 'If the selected file format is invalid.',
  })
  @ApiForbiddenResponse({
    description:
      'If the user does not have the permission to modify the recording file.',
  })
  public async changeRecording(
    @Req() req: Request,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<void> {
    this._roomService.setRecording(req.room!, req.httpID, file?.buffer);
  }

  @UseGuards(WsTokenGuard)
  @Get('/recording')
  @ApiBearerAuth()
  @ApiOkResponse({
    type: Uint8Array,
  })
  @ApiUnauthorizedResponse({
    description: 'If the web socket token invalid.',
  })
  public async getRecording(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<void> {
    res.set('Content-Type', 'plain/text');
    res.set(
      'Content-Disposition',
      `attachment; filename=${req.room!.name}.nlfr`,
    );
    res.send(req.room!.recordingBinary);
  }
}
