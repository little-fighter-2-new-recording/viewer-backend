import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString, IsUUID, MinLength } from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/room-participant.dto';
import { EPermission } from '../../../core-lib/dtos/permission.enum';
import { USERNAME_MIN_LENGTH } from '../../../core-lib/dtos/constants';

export default class RoomParticipantDTO extends BaseDTO {
  @ApiProperty({
    description: '',
  })
  @IsString()
  @MinLength(USERNAME_MIN_LENGTH)
  public username!: string;

  @ApiProperty({
    description: '',
  })
  @IsString()
  @IsUUID()
  public id!: string;

  @ApiProperty({
    description: '',
    enum: Object.values(EPermission).filter(
      (c) => !isNaN(parseInt(c as string)),
    ),
    enumName: 'EPermission',
    default: EPermission.equalRights,
  })
  @IsEnum(EPermission)
  public permission!: EPermission;

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(model: Partial<RoomParticipantDTO>) {
    super(model);
  }
}
