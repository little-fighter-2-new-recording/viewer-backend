import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsBoolean,
  IsNumber,
  IsString,
  IsUUID,
  Max,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/room-joined.dto';
import { RECORDING_TITLE_MIN_LENGTH } from '../../../core-lib/dtos/constants';
import RoomParticipantDTO from './room-participant.dto';
import { EPermission } from '../../../core-lib/dtos/permission.enum';

export default class RoomJoinedDTO extends BaseDTO {
  @ApiProperty({
    description: '',
  })
  @IsString()
  @IsUUID()
  public readonly id!: string;

  @ApiProperty({
    description: '',
  })
  @IsString()
  public readonly wsToken!: string;

  @ApiProperty({
    description: '',
  })
  @IsString()
  @MinLength(RECORDING_TITLE_MIN_LENGTH)
  public readonly title!: string;

  @ApiProperty({
    description: '',
  })
  @IsNumber()
  @Min(1)
  @Max(1000)
  public readonly fps!: number;

  @ApiProperty({
    description: '',
    default: false,
  })
  @IsBoolean()
  public readonly isPause!: boolean;

  @ApiProperty({
    description: '',
    default: false,
  })
  @IsBoolean()
  public readonly isReverse!: boolean;

  @ApiProperty({
    description: '',
  })
  @IsString()
  @IsUUID()
  public readonly ownerID!: string;

  @ApiProperty({
    description: '',
    type: RoomParticipantDTO,
    isArray: true,
  })
  @ValidateNested()
  @IsArray()
  public readonly participants!: RoomParticipantDTO[];

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(
    model: Partial<RoomJoinedDTO> & { permission: EPermission },
  ) {
    super(model);
  }
}
