import { EPermission } from '../../../core-lib/dtos/permission.enum';
import { Socket } from 'socket.io';

export default class Participant {
  public readonly username: string;
  public readonly httpID: string;

  public socket: Socket | undefined;
  public permission?: EPermission;

  public constructor(httpID: string, username: string | undefined) {
    this.httpID = httpID;
    this.username = username ?? 'Firen';
  }
}
