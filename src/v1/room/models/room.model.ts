import { RecordingFormat } from '../../../core-lib/models/recording-format.model';
import { EPermission } from '../../../core-lib/dtos/permission.enum';
import Participant from './participant.model';

const ONE_SECOND_IN_MIL_SECOND = 1000;

export default class Room {
  public id: string;
  public name: string;
  public password?: string;
  public permission: EPermission;

  public isReverse: boolean;
  public isPause: boolean;

  public recording: RecordingFormat | undefined;
  public recordingBinary: Uint8Array | undefined;

  public ownerID: string;
  public readonly creatorID: string;
  public participants: Participant[] = [];

  public lastAction = Date.now();

  private _currentIndex: number;
  private _lastIndexSet = 0;
  private _fps: number;

  public get currentIndex(): number {
    let result = this._currentIndex;

    if (!this.isPause && this._lastIndexSet > 0) {
      const drawInterval = ONE_SECOND_IN_MIL_SECOND / this.fps;
      result += Math.trunc((Date.now() - this._lastIndexSet) / drawInterval);
    }

    return result;
  }

  public set currentIndex(val: number) {
    this._lastIndexSet = Date.now();
    this._currentIndex = val;
  }

  public get fps(): number {
    return this._fps;
  }

  public set fps(val: number) {
    this.currentIndex = this.currentIndex;

    this._fps = val;
  }

  public constructor(
    roomID: string,
    creatorID: string,
    name: string,
    password?: string,
    permission = EPermission.equalRights,
  ) {
    this.id = roomID;
    this.name = name;
    this.password = password;
    this.permission = permission;

    this.creatorID = creatorID;
    this.ownerID = creatorID;

    this._currentIndex = 0;
    this._fps = 30;
    this.isPause = false;
    this.isReverse = false;
  }
}
