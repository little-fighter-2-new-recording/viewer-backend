import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString, IsUUID } from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/room-join.dto';

export default class RoomJoinDTO extends BaseDTO {
  @ApiProperty({
    description: '',
  })
  @IsString()
  @IsUUID()
  public readonly roomID!: string;

  @ApiProperty({
    description: '',
    required: false,
  })
  @IsString()
  @IsOptional()
  public readonly password?: string;

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(model: Partial<RoomJoinDTO>) {
    super(model);
  }
}
