import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsUUID } from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/room-created.dto';

export default class RoomCreatedDTO extends BaseDTO {
  @ApiProperty({
    description: '',
  })
  @IsString()
  @IsUUID()
  public readonly id!: string;

  @ApiProperty({
    description: '',
  })
  @IsString()
  @IsUUID()
  public readonly roomID!: string;

  @ApiProperty({
    description: '',
  })
  @IsString()
  public readonly wsToken!: string;

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(model: Partial<RoomCreatedDTO>) {
    super(model);
  }
}
