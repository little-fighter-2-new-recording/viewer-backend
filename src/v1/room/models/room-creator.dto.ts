import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString, MinLength } from 'class-validator';
import BaseDTO from '../../../core-lib/dtos/room-creator.dto';
import { RECORDING_TITLE_MIN_LENGTH } from '../../../core-lib/dtos/constants';
import { EPermission } from '../../../core-lib/dtos/permission.enum';

export default class RoomCreatorDTO extends BaseDTO {
  @ApiProperty({
    description: '',
  })
  @IsString()
  @MinLength(RECORDING_TITLE_MIN_LENGTH)
  public readonly name!: string;

  @ApiProperty({
    description: '',
    required: false,
  })
  @IsString()
  @IsOptional()
  public readonly password?: string;

  @ApiProperty({
    description: '',
    enum: Object.values(EPermission).filter(
      (c) => !isNaN(parseInt(c as string)),
    ),
    enumName: 'EPermission',
    default: EPermission.equalRights,
  })
  @IsEnum(EPermission)
  public readonly defaultPermission!: EPermission;

  /**
   * This is the constructor of the DTO. Here we set the given parameters as the attribute
   * values of the DTO.
   * @param model This model already represents an object that contains the same information as the actual DTO.
   */
  public constructor(model: Partial<RoomCreatorDTO>) {
    super(model);
  }
}
