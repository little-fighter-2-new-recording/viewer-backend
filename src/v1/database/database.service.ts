import { Injectable } from '@nestjs/common';
import { connect, connection, Error as MError } from 'mongoose';
import { COLLECTIONS } from './collections.constants';
import { ConfigService } from '@nestjs/config';
import {
  REJOIN_DELAY,
  REJOIN_TRIALS,
  TIMEOUT,
} from './default-values.constants';

/**
 * This class (the DatabaseService) is the provider for the primary interaction of the server with the
 * database: The connection to the database. It also handles various other issues related to the connection.
 */
@Injectable()
export class DatabaseService {
  /** Specifies whether it is the first connection between the server and the database.  */
  private _firstConnect = true;

  private _rejoinCounter = 0;

  private readonly _rejoinTrials: number;
  private readonly _rejoinDelayMS: number;
  private readonly _timeout: number;

  /**
   * Constructor for the DatabaseService which sets all the events for the database connection.
   */
  public constructor(private readonly _configService: ConfigService) {
    this._rejoinTrials =
      this._configService.get<number>('db.rejoin_trials') ?? REJOIN_TRIALS;
    this._rejoinDelayMS =
      this._configService.get<number>('db.rejoin_delay') ?? REJOIN_DELAY;
    this._timeout = this._configService.get<number>('db.timeout') ?? TIMEOUT;

    this.setConnectionEvents();
  }

  /**
   * Handles the connection event "connected".
   */
  /* istanbul ignore next */
  private static listenerConnected(): void {
    console.info('Connection established successfully, no errors encountered.');
  }

  /**
   * This function generates all required collections.
   * This function is NOT required for the functionality, because Mongoose generates the collection itself.
   * This function should help with the development as long as all the collections are not available.
   */
  private static async checkCollection(): Promise<void> {
    // iterates over the collection names. get the values of dic, adds an "s" and transform collection name to lower case
    for (const collectionName of Object.values(COLLECTIONS).map(
      (val) => `${val.toLocaleLowerCase()}s`,
    )) {
      try {
        connection.db
          .listCollections({
            name: collectionName,
          })
          .next((error, result) => {
            if (!result) connection.db.createCollection(collectionName);

            if (error)
              console.warn(
                `Collection creation failed. ${collectionName} exists already?`,
              );
          });
      } catch (e) {
        console.warn(
          `Collection creation failed. ${collectionName} exists already?`,
        );
      }
    }
  }

  /**
   * Function which aims to connect the server to the database.
   */
  public async connectDatabase(): Promise<void> {
    const firstConnect = this._firstConnect;
    this._firstConnect = false;

    const url = this.getURL(this._configService.get<string>('db.args'));
    try {
      await connect(url, {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        // connectTimeoutMS: TIMEOUT_MS,
        // socketTimeoutMS: TIMEOUT_MS,
        serverSelectionTimeoutMS: this._timeout,
      });

      if (firstConnect) await DatabaseService.checkCollection();

      this._rejoinCounter = 0;
    } catch (e) {
      if (e instanceof MError) {
        if (!this.rejoin() && this._rejoinCounter >= this._rejoinTrials)
          console.log('The database is not accessible.');

        return;
      }
      console.warn(`Connection ${e}`);
    }
  }

  /**
   * Function to close the connection to the database.
   */
  public async disconnectDatabase(): Promise<void> {
    await connection.close();
  }

  /**
   * Sets all helpful events for a connection, including a listener in the event the connection
   * has been successful, a bind in the event that the connection has been terminated and an error if
   * there has been an error while attempting to connect.
   */
  private setConnectionEvents(): void {
    connection.removeAllListeners();
    connection.once('connected', DatabaseService.listenerConnected);
    connection.once('disconnected', this.listenerDisconnected.bind(this));
    connection.once('error', this.listenerOnError.bind(this));
  }

  /**
   * Handles the connection event "disconnected".
   */
  /* istanbul ignore next */
  private listenerDisconnected(): void {
    console.info('Connection to database terminated, attempting to reconnect.');
    this.connectDatabase();
  }

  /**
   * Handles the connection event "error".
   */
  /* istanbul ignore next */
  private listenerOnError(): void {
    console.info(
      'Red Alert. Encountered an error while attempting to connect.',
    );

    if (connection.readyState === 0) this.rejoin();
  }

  private rejoin(): boolean {
    if (this._rejoinCounter < this._rejoinTrials) {
      // istanbul ignore next
      if (connection.readyState === 0) {
        console.info('Rejoin in few seconds...');
        setTimeout(this.connectDatabase.bind(this), this._rejoinDelayMS);
        ++this._rejoinCounter;
        return true;
      }
    }

    return false;
  }

  /**
   * This function creates the connection uri to a database.
   * @param args The optional arguments for the connection.
   */
  private getURL(args?: string): string {
    const protocol = this._configService.get<string>('db.protocol') ?? '';
    const uri = this._configService.get<string>('db.url') ?? '';
    const portNumber = this._configService.get<number>('db.port') ?? '';
    const collectionName =
      this._configService.get<string>('db.collection') ?? '';
    const argsPart = args ? `?${args}` : '';

    let port = '';
    if (portNumber) port = `:${portNumber.toString()}`;
    return `${protocol}://${uri}${port}/${collectionName}${argsPart}`;
  }
}
