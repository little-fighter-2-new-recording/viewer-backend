import { HookNextFunction, Model, model, Schema } from 'mongoose';
import { compare, hash } from 'bcrypt';
import validator from 'validator';
// it is not possible to install the types because it requires to mongoose types.
// but mongoose and mongoose types are not compilable...
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as UniqueValidator from 'mongoose-unique-validator';
import UserDocument from '../documents/user.document';
import { COLLECTION_USERS } from '../../database/collections.constants';

const PASSWORD_HASH_ROUNDS = 8;

/**
 * Model expansions for the Model of User so that the static methods are usable via the user model
 */
interface IUserModel extends Model<UserDocument> {
  /**
   * Method to find a user by his email address and his password (serves as a semi-login function
   * where it compares all the user information that is given with the found one).
   * @param email This is the email address that the user provides.
   * @param password This is the unhashed password that the user provides.
   * @return Returns a user (real user) as the result of the process.
   */
  findByEmailAndPassword(
    email: string,
    password: string,
  ): Promise<UserDocument>;

  /**
   * Method to find a user by his username and password (serves as a semi-login function
   * where it compares all the user information that is given with the found one).
   * @param username Is the username given by the user.
   * @param password Is the unhashed password given that will be compared with the hashed one.
   * @return Returns a user (real user) as the result of the process.
   */
  findByUsernameAndPassword(
    username: string,
    password: string,
  ): Promise<UserDocument>;
}

const UserSchema = new Schema<UserDocument>(
  {
    username: {
      type: Schema.Types.String,
      required: true,
      trim: true,
      unique: true,
    },
    email: {
      type: Schema.Types.String,
      unique: true,
      lowercase: true,
      validate: {
        validator: (value: string) => validator.isEmail(value),
        message: (props: { value: string }) =>
          `${props.value} is not a valid email address!`,
      },
    },
    password: {
      type: Schema.Types.String,
      required: true,
    },
    tokens: [
      {
        type: Schema.Types.String,
      },
    ],
  },
  { toObject: { virtuals: true }, toJSON: { virtuals: true } },
);

UserSchema.plugin(UniqueValidator, {
  message: 'Error, expected {PATH} to be unique.',
});

/**
 * Before saving the user password it will be hashed and then stored.
 */
UserSchema.pre<UserDocument>(
  'save',
  async function (this: UserDocument, next: HookNextFunction) {
    if (this.isModified('password'))
      this.password = await hash(this.password, PASSWORD_HASH_ROUNDS);

    next();
  },
);

/**
 * Method to find a user by his email address and his password (serves as a semi-login function
 * where it compares all the user information that is given with the found one).
 * @param email This is the email address that the user provides.
 * @param password This is the unhashed password that the user provides.
 * @return Returns a user (real user) as the result of the process.
 */
UserSchema.statics.findByEmailAndPassword = async function (
  email: string,
  password: string,
): Promise<UserDocument> {
  const user = await this.findOne({ email: email.toLowerCase() });
  if (!user || !(await compare(password, user.password)))
    throw new Error('Unable to login');

  return user;
};

/**
 * Method to find a user by his username and password (serves as a semi-login function
 * where it compares all the user information that is given with the found one).
 * @param username Is the username given by the user.
 * @param password Is the unhashed password given that will be compared with the hashed one.
 * @return Returns a user (real user) as the result of the process.
 */
UserSchema.statics.findByUsernameAndPassword = async function (
  username: string,
  password: string,
): Promise<UserDocument> {
  const user: UserDocument | null = await this.findOne({ username });
  if (!user || !(await compare(password, user.password)))
    throw new Error('Unable to login');

  return user;
};

/**
 * The model for the user is set and exported for use within classes and functions.
 */
const UserDBM: IUserModel = model<UserDocument, IUserModel>(
  COLLECTION_USERS,
  UserSchema,
);
export default UserDBM;
