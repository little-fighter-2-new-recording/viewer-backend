import { model, Schema } from 'mongoose';
import RecordingDocument from '../documents/recording.document';
import {
  COLLECTION_RECORDINGS,
  COLLECTION_USERS,
} from '../../database/collections.constants';

const RecordingSchema = new Schema<RecordingDocument>(
  {
    uploader: {
      type: Schema.Types.ObjectId,
      ref: COLLECTION_USERS,
      required: true,
    },
    author: {
      type: Schema.Types.String,
      required: true,
      trim: true,
    },
    title: {
      type: Schema.Types.String,
      required: true,
      trim: true,
    },
    file: {
      type: Schema.Types.Buffer,
      required: true,
    },
    uploadDate: {
      type: Schema.Types.Number,
    },
    fileSize: {
      type: Schema.Types.Number,
    },
  },
  { toObject: { virtuals: true }, toJSON: { virtuals: true } },
);

/**
 * The model for the user is set and exported for use within classes and functions.
 */
const RecordingDBM = model<RecordingDocument>(
  COLLECTION_RECORDINGS,
  RecordingSchema,
);
export default RecordingDBM;
