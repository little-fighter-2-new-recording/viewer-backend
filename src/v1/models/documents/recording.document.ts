import { Document } from 'mongoose';
import UserDocument from './user.document';

/**
 * Represents a valid document object of a record.
 */
export default interface RecordingDocument extends Document {
  uploader: UserDocument;
  author: string;
  title: string;
  uploadDate: number;
  file: Buffer;
  fileSize: number;
}
