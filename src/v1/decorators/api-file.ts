import { ApiBody } from '@nestjs/swagger';

/**
 *
 * @param fileName
 * @constructor
 * @source https://github.com/nestjs/swagger/issues/417#issuecomment-562869578
 */
export const ApiFile = (fileName = 'file'): MethodDecorator => (
  target: any,
  propertyKey: string | symbol,
  descriptor: PropertyDescriptor,
): void => {
  ApiBody({
    schema: {
      type: 'object',
      properties: {
        [fileName]: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })(target, propertyKey, descriptor);
};
