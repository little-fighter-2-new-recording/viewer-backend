import { SetUserAgentMiddleware } from './set-user-agent.middleware';

describe('SetUserAgentMiddleware', () => {
  it('should be defined', () => {
    expect(new SetUserAgentMiddleware()).toBeDefined();
  });
});
