declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace NodeJS {
    /**
     * Expands the interface `ProcessEnv` from NodeJS.
     * The variable `process.env` reference an object with this interface.
     */
    interface ProcessEnv {
      NODE_ENV: undefined | 'development' | 'production';
    }

    /**
     * Expands the interface `Process` from NodeJS.
     */
    interface Process {
      /**
       * Specifies whether the node server is running in development mode.
       */
      IS_DEVELOPMENT: boolean;

      PROJECT_ROOT: string;
    }
  }
}

export {};
