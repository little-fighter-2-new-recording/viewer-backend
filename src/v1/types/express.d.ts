// eslint-disable-next-line @typescript-eslint/no-unused-vars
import UserDocument from '../models/documents/user.document';

declare global {
  namespace Express {
    /**
     * Expand the Express Request interface
     */
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface Request extends UserInformation {}
  }
}
