import UserDocument from '../models/documents/user.document';
import Room from '../room/models/room.model';

declare global {
  interface UserInformation {
    user?: UserDocument;
    token?: string;
    userAgent: string;
    httpID: string;
    room?: Room;
  }
}

export {};
