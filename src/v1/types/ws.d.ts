declare module 'socket.io' {
  /**
   * Expands the interface `Process` from NodeJS.
   */
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface Socket extends UserInformation {}
}

export {};
