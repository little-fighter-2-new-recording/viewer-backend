import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class OptionalPipe<T extends PipeTransform> implements PipeTransform {
  public constructor(private pipe: new () => T) {}

  public async transform(
    value: any, // eslint-disable-line @typescript-eslint/explicit-module-boundary-types
    metadata: ArgumentMetadata,
  ): Promise<T | undefined> {
    if (value === undefined || value === null) return;

    const pipe = new this.pipe();
    return pipe.transform(value, metadata);
  }
}
