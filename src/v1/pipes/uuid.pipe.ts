import { BadGatewayException, Injectable, PipeTransform } from '@nestjs/common';
import { validate } from 'uuid';

@Injectable()
export class UuidPipe implements PipeTransform {
  public transform(value: string): string {
    if (!validate(value)) throw new BadGatewayException('The id is invalid!');

    return value;
  }
}
