import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import BinaryParser from '../../core-lib/parsers/binary-parser';

@Injectable()
export class Lf2RecordingFormatPipe implements PipeTransform {
  public transform(value: Express.Multer.File): boolean | undefined {
    try {
      new BinaryParser(value.buffer).parse();

      return true; // no exceptions => all fine
    } catch (e) {
      console.warn(e);
      throw new BadRequestException(
        'The submitted file is invalid. It could not be determined that it is a recording file (`nlfr`).',
      );
    }
  }
}
