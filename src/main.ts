import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { SetUserAgentMiddleware } from './v1/middlewares/set-user-agent.middleware';
import { AuthWsIoAdapter } from './v1/adapter/auth-ws-io.adapter';

process.IS_DEVELOPMENT = !(process.env.NODE_ENV === 'production');
process.PROJECT_ROOT = __dirname;

const PORT = process.env.PORT || 9000;
const ORIGIN_URL = process.IS_DEVELOPMENT
  ? /http:\/\/localhost:4200(\/.*)?/
  : /https?:\/\/viewer\.lf-archive\.de(\/?|\/.*)$/;

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    credentials: true,
    origin: ORIGIN_URL,
  });
  app.use(SetUserAgentMiddleware.use);
  app.useWebSocketAdapter(new AuthWsIoAdapter(app, ORIGIN_URL));

  /* Swagger */
  const options = new DocumentBuilder()
    .setTitle('API of "Easier LF2 Replay Viewer (Online)"')
    .setDescription(
      'The API provides endpoints for <i>Easier LF2 Replay Viewer (Online)<i>.',
    )
    .setContact('Team', '', 'contact@lui-studio.net')
    .addBearerAuth();

  options
    .addTag('v1', '')
    .addTag(
      'recording',
      'Responsible for handling all actions and requests concerning the recordings.',
    );

  const doc = options.build();
  const document = SwaggerModule.createDocument(app, doc);
  SwaggerModule.setup('/api/v1/', app, document, {
    customCss: '.swagger-ui .topbar { display: none }',
  });

  await app.listen(PORT);
}
bootstrap();
