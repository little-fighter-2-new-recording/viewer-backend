import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { RecordingModule } from './v1/recording/recording.module';
import configuration from './configuration';
import { DatabaseModule } from './v1/database/database.module';
import { UserModule } from './v1/user/user.module';
import { RoomModule } from './v1/room/room.module';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [configuration] }),
    RecordingModule,
    DatabaseModule,
    UserModule,
    RoomModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
