import { readFileSync } from 'fs';
import { load } from 'js-yaml';
import { join } from 'path';

const YAML_CONFIG_PRODUCTION = 'conf.prod.yml';
const YAML_CONFIG_DEVELOPMENT = 'conf.local.yml';

export default (): Record<string, any> => {
  let configFile: string;

  if (process.IS_DEVELOPMENT)
    configFile = join(__dirname, 'config', YAML_CONFIG_DEVELOPMENT);
  else configFile = join(__dirname, 'config', YAML_CONFIG_PRODUCTION);

  return load(readFileSync(configFile, 'utf8')) as Record<string, any>;
};
