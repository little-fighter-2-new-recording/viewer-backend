# multi-stage

# select alpine as base image and rename to "builder"
FROM node:13.7.0-alpine AS builder

ENV NODE_ENV "production"

WORKDIR /build

COPY package.json yarn.lock dist ./
# install all dependencies and ignore "NODE_ENV" and use the flag
RUN yarn install --prod



# Container
FROM node:13.7.0-alpine

# set port to 80
ENV NODE_ENV "production"
ENV PORT 80

WORKDIR /server

COPY --from=builder /build .

# indicates the ports on which a container listens for connections.
EXPOSE ${PORT}

# specifies what command to run within the container
CMD ["node", "/server/main.js"]
